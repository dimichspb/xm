<?php

/* @var $this yii\web\View */
/* @var $model \app\entities\request\Request */

use yii\helpers\Html;

$this->title = 'Email success';
$this->params['breadcrumbs'][] = ['label' => 'New request', 'url' => ['start']];
$this->params['breadcrumbs'][] = ['label' => 'Request results', 'url' => ['result', 'id' => $model->getId()->getValue()]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        The email has been sent
    </p>
</div>
