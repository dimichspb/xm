<?php

/* @var $this yii\web\View */
/* @var $model app\forms\RequestCreateForm */
/* @var $symbolDataProvider \yii\data\DataProviderInterface */
/* @var $selectedSymbol \app\entities\symbol\Symbol */

use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use yii\web\JsExpression;

$this->title = 'New request';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="exercise-start">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Please fill the following form to request historical data
    </p>

    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin(['id' => 'exercise-form']); ?>

            <?= $form->field($model, 'symbol_id')->widget(Select2::classname(), [
                'initValueText' => $selectedSymbol? $selectedSymbol->getCode()->getValue(): '',
                'options' => ['placeholder' => 'Search for a symbol ...'],
                'pluginOptions' => [
                    'allowClear' => false,
                    'minimumInputLength' => 2,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => \yii\helpers\Url::toRoute(['symbol/index']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                        'processResults' => new JsExpression('function (data) {
                        return {
                            results: data
                        };
                    }'
                        ),
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(symbol) { return symbol.code; }'),
                    'templateSelection' => new JsExpression('function (symbol) { return symbol.code; }'),
                ],
            ]); ?>

            <?= $form->field($model, 'start_date')->widget(DatePicker::className(), [
                'dateFormat' => 'yyyy-MM-dd',
                'containerOptions' => [
                    'readonly' => true,
                ]
            ]) ?>

            <?= $form->field($model, 'end_date')->widget(DatePicker::className(), [
                'dateFormat' => 'yyyy-MM-dd',
                'containerOptions' => [
                    'readonly' => true,
                ]
            ]) ?>

            <?= $form->field($model, 'email')->textInput(['type' => 'email']) ?>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'submit-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>
