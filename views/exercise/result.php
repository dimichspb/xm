<?php

/* @var $this yii\web\View */
/* @var $model \app\entities\request\Request */
/* @var $dataProvider \yii\data\DataProviderInterface */

use app\entities\quote\Quote;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

$this->title = 'Request result';
$this->params['breadcrumbs'][] = ['label' => 'New request', 'url' => ['start']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="exercise-result">
    <div class="row">
        <div class="col-md-12">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-md-1">
            <?= Html::a('Chart', ['chart', 'id' => $model->getId()->getValue()], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="col-md-1">
            <?= Html::beginForm(['email', 'id' => $model->getId()->getValue()], 'post') ?>
            <?= Html::submitButton(
                'Email',
                ['class' => 'btn btn-info']
            ) ?>
            <?= Html::endForm() ?>
        </div>
    </div>
    <hr>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' =>'id',
                'value' => $model->getId()->getValue(),
            ],
            [
                'attribute' => 'symbol',
                'value' => $model->symbol->getCode()->getValue(),
            ],
            [
                'attribute' => 'start_date',
                'value' => $model->getStartDate()->getValue(),
            ],
            [
                'attribute' => 'end_date',
                'value' => $model->getEndDate()->getValue(),
            ],
        ]
    ]); ?>

    <?php Pjax::begin([
        'id' => 'quote-index-container',
        'enablePushState' => false,
    ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'date',
                'value' => function(Quote $model) {
                    return $model->getDate()->getValue();
                },
            ],
            [
                'attribute' => 'open',
                'value' => function(Quote $model) {
                    return $model->getOpen()->getValue();
                },
            ],
            [
                'attribute' => 'high',
                'value' => function(Quote $model) {
                    return $model->getHigh()->getValue();
                },
            ],
            [
                'attribute' => 'low',
                'value' => function(Quote $model) {
                    return $model->getLow()->getValue();
                },
            ],
            [
                'attribute' => 'close',
                'value' => function(Quote $model) {
                    return $model->getClose()->getValue();
                },
            ],
            [
                'attribute' => 'volume',
                'value' => function(Quote $model) {
                    return $model->getVolume()->getValue();
                },
            ],
        ]
    ]); ?>
    <?php Pjax::end(); ?>
</div>
