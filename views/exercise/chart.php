<?php

/* @var $this yii\web\View */
/* @var $model \app\entities\request\Request */
/* @var $data array */

use scotthuangzl\googlechart\GoogleChart;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

$this->title = 'Request chart';
$this->params['breadcrumbs'][] = ['label' => 'New request', 'url' => ['start']];
$this->params['breadcrumbs'][] = ['label' => 'Request results', 'url' => ['result', 'id' => $model->getId()->getValue()]];

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="exercise-chart">
    <div class="row">
        <div class="col-md-12">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-md-1">
            <?= Html::a('Table', ['result', 'id' => $model->getId()->getValue()], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="col-md-1">
            <?= Html::beginForm(['email', 'id' => $model->getId()->getValue()], 'post') ?>
            <?= Html::submitButton(
                'Email',
                ['class' => 'btn btn-info']
            ) ?>
            <?= Html::endForm() ?>
        </div>
    </div>
    <hr>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' =>'id',
                'value' => $model->getId()->getValue(),
            ],
            [
                'attribute' => 'symbol',
                'value' => $model->symbol->getCode()->getValue(),
            ],
            [
                'attribute' => 'start_date',
                'value' => $model->getStartDate()->getValue(),
            ],
            [
                'attribute' => 'end_date',
                'value' => $model->getEndDate()->getValue(),
            ],
        ]
    ]); ?>

    <?php Pjax::begin([
        'id' => 'quote-index-container',
        'enablePushState' => false,
    ]); ?>
    <?= GoogleChart::widget([
        'visualization' => 'CandlestickChart',
        'data' => $data,
        'options' => [
            'title' => $model->symbol->getName()->getValue(),
            'height' => 500,
        ],
    ]); ?>


    <?php Pjax::end(); ?>
</div>
