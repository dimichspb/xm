<?php

use yii\db\Migration;

/**
 * Class m171212_120136_alter_id_columns
 */
class m171212_120136_alter_id_columns extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_request_symbol_id_symbol_id', '{{request}}');
        $this->alterColumn('{{request}}', 'id', $this->char(36)->notNull());
        $this->alterColumn('{{request}}', 'symbol_id', $this->char(36)->notNull());
        $this->alterColumn('{{symbol}}', 'id', $this->char(36)->notNull());
        $this->addForeignKey('fk_request_symbol_id_symbol_id', '{{request}}', 'symbol_id', '{{symbol}}', 'id', 'RESTRICT', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
