<?php

use yii\db\Migration;

/**
 * Class m171212_120651_alter_code_column
 */
class m171212_120651_alter_code_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('{{symbol}}', 'code', $this->char(10)->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        ;
    }
}
