<?php

use yii\db\Migration;

/**
 * Class m171212_075931_create_table_symbol
 */
class m171212_075931_create_table_symbol extends Migration
{
    public $tableName = '{{symbol}}';
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'symbol' => $this->string(6)->notNull(),
            'name' => $this->string(    255)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
