<?php

use yii\db\Migration;

/**
 * Class m171212_080528_create_table_request
 */
class m171212_080528_create_table_request extends Migration
{
    public $tableName = '{{request}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'symbol_id' => $this->integer()->notNull(),
            'start_date' => $this->dateTime()->notNull(),
            'end_date' => $this->dateTime()->notNull(),
            'email' => $this->string(255)->notNull(),
        ]);

        $this->addForeignKey('fk_request_symbol_id_symbol_id', $this->tableName, 'symbol_id', '{{symbol}}', 'id', 'RESTRICT', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
