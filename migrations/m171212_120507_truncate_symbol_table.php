<?php

use yii\db\Migration;

/**
 * Class m171212_120507_truncate_symbol_table
 */
class m171212_120507_truncate_symbol_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_request_symbol_id_symbol_id', '{{request}}');
        $this->truncateTable('{{request}}');
        $this->truncateTable('{{symbol}}');
        $this->addForeignKey('fk_request_symbol_id_symbol_id', '{{request}}', 'symbol_id', '{{symbol}}', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }

}
