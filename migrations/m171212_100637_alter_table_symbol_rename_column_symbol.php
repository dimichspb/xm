<?php

use yii\db\Migration;

/**
 * Class m171212_100637_alter_table_symbol_rename_column_symbol
 */
class m171212_100637_alter_table_symbol_rename_column_symbol extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('{{symbol}}', 'symbol', 'code');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->renameColumn('{{symbol}}', 'code', 'symbol');
    }
}
