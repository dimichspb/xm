<?php
namespace app\entities;

use Assert\Assertion;

trait ConstructTrait
{
    private $value;

    public function __construct($value = null)
    {
        Assertion::notEmpty($value);

        $this->value = $value;
    }
}