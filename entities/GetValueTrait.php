<?php
namespace app\entities;

trait GetValueTrait
{
    private $value;

    public function getValue()
    {
        return $this->value;
    }
}