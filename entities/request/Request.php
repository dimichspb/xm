<?php

namespace app\entities\request;

use app\entities\Email;
use app\entities\Entity;
use app\entities\EventTrait;
use app\entities\InstantiateTrait;
use app\entities\LazyLoadTrait;
use app\entities\request\events\RequestCreatedEvent;
use app\entities\symbol\Symbol;
use app\entities\symbol\SymbolId;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use ProxyManager\Proxy\LazyLoadingInterface;
use Yii;

/**
 * This is the model class for table "request".
 *
 * @property Symbol $symbol
 */
class Request extends Entity
{
    use EventTrait, InstantiateTrait, LazyLoadTrait;

    private $id;
    private $symbol_id;
    private $start_date;
    private $end_date;
    private $email;


    public function __construct(RequestId $id, SymbolId $symbolId, StartDate $startDate, EndDate $endDate, Email $email)
    {
        $this->id = $id;
        $this->symbol_id = $symbolId;
        $this->start_date = $startDate;
        $this->end_date = $endDate;
        $this->email = $email;

        $this->recordEvent(new RequestCreatedEvent($this->id));
        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{request}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SaveRelationsBehavior::className(),
                'relations' => ['symbol'],
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    public function setSymbolId(SymbolId $symbolId)
    {
        $this->symbol_id = $symbolId;
    }

    /**
     * @return SymbolId
     */
    public function getSymbolId()
    {
        return $this->symbol_id;
    }

    public function setStartDate(StartDate $startDate)
    {
        $this->start_date = $startDate;
    }

    /**
     * @return StartDate
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    public function setEndDate(EndDate $endDate)
    {
        $this->end_date = $endDate;
    }

    /**
     * @return EndDate
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    public function setEmail(Email $email)
    {
        $this->email = $email;
    }

    /**
     * @return Email
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function afterFind()
    {
        $this->id = new RequestId(
            $this->getAttribute('id')
        );
        $this->symbol_id = new SymbolId(
            $this->getAttribute('symbol_id')
        );
        $this->start_date = new StartDate(
            $this->getAttribute('start_date')
        );
        $this->end_date = new EndDate(
            $this->getAttribute('end_date')
        );
        $this->email = new Email(
            $this->getAttribute('email')
        );

        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        $this->setAttribute('id', $this->getId()->getValue());
        $this->setAttribute('symbol_id', $this->getSymbolId()->getValue());
        $this->setAttribute('start_date', $this->getStartDate()->getValue());
        $this->setAttribute('end_date', $this->getEndDate()->getValue());
        $this->setAttribute('email', $this->getEmail()->getValue());

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSymbol()
    {
        return $this->hasOne(Symbol::class, ['id' => 'symbol_id']);
    }
}
