<?php
namespace app\entities\request\events;

use app\entities\request\RequestId;

class RequestCreatedEvent
{
    public $requestId;

    public function __construct(RequestId $requestId)
    {
        $this->requestId = $requestId;
    }
}