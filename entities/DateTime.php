<?php
namespace app\entities;

class DateTime
{
    use GetValueTrait, ConstructTrait, IsEqualTrait;

    public function getFormattedValue()
    {
        $value = $this->getValue();
        $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s', $value);
        return $dateTime->format('M d, Y');
    }

    public function getShortDate()
    {
        $value = $this->getValue();
        $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s', $value);
        return $dateTime->format('Y-m-d');
    }
}