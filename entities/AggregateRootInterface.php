<?php

namespace app\entities;

interface AggregateRootInterface
{
    /**
     * @return Id
     */
    public function getId();

    /**
     * @return array
     */
    public function releaseEvents();
}