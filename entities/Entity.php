<?php
namespace app\entities;

use yii\db\ActiveRecord;

abstract class Entity extends ActiveRecord implements AggregateRootInterface
{
    use EventTrait, InstantiateTrait, LazyLoadTrait;
}