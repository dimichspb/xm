<?php
namespace app\entities;

trait IsEqualTrait
{
    public function isEqualTo(self $other)
    {
        return $this->getValue() === $other->getValue();
    }
}