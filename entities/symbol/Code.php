<?php
namespace app\entities\symbol;

use app\entities\ConstructTrait;
use app\entities\GetValueTrait;
use app\entities\IsEqualTrait;

class Code
{
    use GetValueTrait, ConstructTrait, IsEqualTrait;
}