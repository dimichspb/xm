<?php
namespace app\entities\symbol\events;

use app\entities\symbol\SymbolId;

class SymbolCreatedEvent
{
    public $symbolId;

    public function __construct(SymbolId $symbolId)
    {
        $this->symbolId = $symbolId;
    }
}