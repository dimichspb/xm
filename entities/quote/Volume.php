<?php
namespace app\entities\quote;

use app\entities\ConstructTrait;
use app\entities\GetValueTrait;
use app\entities\IsEqualTrait;

class Volume
{
    use GetValueTrait, ConstructTrait, IsEqualTrait;
}