<?php
namespace app\entities\quote\events;

use app\entities\quote\QuoteId;

class QuoteCreatedEvent
{
    public $quoteId;

    public function __construct(QuoteId $quoteId)
    {
        $this->quoteId = $quoteId;
    }
}