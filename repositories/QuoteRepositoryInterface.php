<?php
namespace app\repositories;

use app\entities\quote\Quote;
use app\entities\quote\QuoteId;
use app\entities\request\Request;

interface QuoteRepositoryInterface
{
    public function get(QuoteId $id);
    public function all(Request $request);
    public function add(Quote $quote);
    public function save(Quote $quote);
    public function remove(Quote $quote);
    public function nextId();

}