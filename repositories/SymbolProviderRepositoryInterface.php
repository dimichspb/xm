<?php
namespace app\repositories;

use app\entities\symbol\Symbol;
use app\entities\symbol\SymbolId;

interface SymbolProviderRepositoryInterface
{
    public function get(SymbolId $id);
    public function all(array $conditions = []);
    public function add(Symbol $symbol);
    public function save(Symbol $symbol);
    public function remove(Symbol $symbol);
    public function nextId();

}