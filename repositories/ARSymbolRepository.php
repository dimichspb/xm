<?php
namespace app\repositories;

use app\entities\symbol\Symbol;
use app\entities\symbol\SymbolId;
use app\exceptions\NotFoundException;
use Ramsey\Uuid\Uuid;

class ARSymbolRepository extends BaseSymbolRepository
{
    public function get(SymbolId $id)
    {
        if (!$symbol = Symbol::findOne($id->getValue())) {
            throw new NotFoundException('Symbol not found.');
        }
        return $symbol;
    }

    public function all(array $conditions = [])
    {
        return Symbol::find()->where($conditions)->orderBy('code')->all();
    }

    public function add(Symbol $symbol)
    {
        if (!$symbol->insert()) {
            throw new \RuntimeException('Adding error.');
        }
    }

    public function save(Symbol $symbol)
    {
        if (!$symbol->update() === false) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Symbol $symbol)
    {
        if (!$symbol->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    public function nextId()
    {
        return Uuid::uuid4()->toString();
    }
}