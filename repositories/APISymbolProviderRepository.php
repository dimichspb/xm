<?php
namespace app\repositories;

use app\dto\SymbolCreateDto;
use app\entities\symbol\Symbol;
use app\entities\symbol\SymbolId;
use app\entities\request\Request;
use app\exceptions\NotFoundException;
use app\exceptions\NotSupportedException;
use app\factories\SymbolFactoryInterface;
use app\services\SymbolServiceInterface;
use Assert\Assert;
use Assert\Assertion;
use Ramsey\Uuid\Uuid;
use yii\base\BaseObject;
use yii\httpclient\Client;

class APISymbolProviderRepository implements SymbolProviderRepositoryInterface
{
    use APIFetchTrait;

    private $factory;

    public function __construct($url, $method = null, $proxy = null, $options = [], SymbolFactoryInterface $factory)
    {
        $this->client = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);
        $this->setUrl($url);
        $this->setMethod($method);
        $this->setProxy($proxy);
        $this->setOptions($options);

        $this->factory = $factory;
    }

    public function get(SymbolId $id)
    {
        throw new NotSupportedException();
    }

    public function all(array $conditions = [])
    {
        $array = $this->setData($conditions)->getArray();

        if (count($array) > 1) {
            reset($array);
            $key = key($array);
            unset($array[$key]);
        }

        foreach ($array as $index => $item) {
            $dto = new SymbolCreateDto();
            $dto->id = new SymbolId($this->nextId());
            $dto->code = isset($item[0])? $item[0]: null;
            $dto->name = isset($item[1])? $item[1]: null;

            try {
                $array[$index] = $this->factory->create($dto);
            } catch (\Exception $exception) {
                // wrong format of input data
                unset($array[$index]);
                continue;
            }

        }

        return $array;
    }

    public function add(Symbol $symbol)
    {
        throw new NotSupportedException();
    }

    public function save(Symbol $symbol)
    {
        throw new NotSupportedException();
    }

    public function remove(Symbol $symbol)
    {
        throw new NotSupportedException();
    }

    public function nextId()
    {
        return Uuid::uuid4()->toString();
    }
}