<?php
namespace app\repositories;

use app\exceptions\ProviderNotAvailableException;
use yii\httpclient\Client;

trait APIFetchTrait
{
    private $client;
    private $url;
    private $method = 'get';
    private $proxy;
    private $data = [];
    private $options;

    public function setUrl($url)
    {
        if ($url) {
            $this->url = $url;
        }
        return $this;
    }

    public function setMethod($method)
    {
        if ($method) {
            $this->method = $method;
        }
        return $this;
    }

    public function setProxy($proxy)
    {
        if ($proxy) {
            $this->proxy = $proxy;
        }
        return $this;
    }

    public function setData(array $data)
    {
        if ($data) {
            $this->data = array_merge($this->data, $data);
        }
        return $this;
    }

    public function setOptions(array $options)
    {
        if ($this->proxy) {
            $this->options['proxy'] = $this->proxy;
        }
        if ($options) {
            $this->options = array_merge($this->options, $options);
        }
    }

    public function getArray()
    {
        $response = $this
            ->client
            ->createRequest()
            ->setMethod($this->method)
            ->setUrl($this->url)
            ->setOptions($this->options)
            ->setData($this->data)
            ->send();

        if (!$response->isOk) {
            throw new ProviderNotAvailableException('API provider is not available');
        }
        $data = array_map("str_getcsv", preg_split('/\r*\n+|\r+/', $response->getContent()));

        return $data;
    }
}