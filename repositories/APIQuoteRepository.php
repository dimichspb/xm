<?php
namespace app\repositories;

use app\dto\QuoteCreateDto;
use app\entities\quote\Quote;
use app\entities\quote\QuoteId;
use app\entities\request\Request;
use app\entities\symbol\Symbol;
use app\exceptions\NotFoundException;
use app\exceptions\NotSupportedException;
use app\factories\QuoteFactoryInterface;
use app\services\SymbolServiceInterface;
use Assert\Assert;
use Assert\Assertion;
use Ramsey\Uuid\Uuid;
use yii\httpclient\Client;

class APIQuoteRepository extends BaseQuoteRepository
{
    use APIFetchTrait;

    private $factory;

    public function __construct($url, $method = null, $proxy = null, $options = [], QuoteFactoryInterface $factory)
    {
        $this->client = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);
        $this->setUrl($url);
        $this->setMethod($method);
        $this->setProxy($proxy);
        $this->setOptions($options);

        $this->factory = $factory;
    }

    public function get(QuoteId $id)
    {
        throw new NotSupportedException();
    }

    public function all(Request $request)
    {
        $conditions = [
            'q' => $request->symbol->getCode()->getValue(),
            'startdate' => $request->getStartDate()->getFormattedValue(),
            'enddate'   => $request->getEndDate()->getFormattedValue(),
        ];

        $array = $this->setData($conditions)->getArray();

        if (count($array) > 1) {
            reset($array);
            $key = key($array);
            unset($array[$key]);
        }

        foreach ($array as $index => $item) {
            $dto = new QuoteCreateDto();
            $dto->id = new QuoteId($this->nextId());
            $dto->symbol_id = $request->getSymbolId()->getValue();
            if (isset($item[0])) {
                $value = $item[0];
                $date = \DateTime::createFromFormat('d-M-y', $value);
                $dto->date = $date->format('Y-m-d H:i:s');
            } else {
                $dto->date = null;
            }

            $dto->open = isset($item[1])? $item[1]: null;
            $dto->high = isset($item[2])? $item[2]: null;
            $dto->low  = isset($item[3])? $item[3]: null;
            $dto->close = isset($item[4])? $item[4]: null;
            $dto->volume = isset($item[5])? $item[5]: null;

            try {
                $array[$index] = $this->factory->create($dto);
            } catch (\Exception $exception) {
                unset($array[$index]);
                // wrong format of input data
                continue;
            }
        }

        return $array;
    }

    public function add(Quote $quote)
    {
        throw new NotSupportedException();
    }

    public function save(Quote $quote)
    {
        throw new NotSupportedException();
    }

    public function remove(Quote $quote)
    {
        throw new NotSupportedException();
    }

    public function nextId()
    {
        return Uuid::uuid4()->toString();
    }
}