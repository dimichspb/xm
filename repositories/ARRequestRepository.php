<?php
namespace app\repositories;

use app\entities\request\Request;
use app\entities\request\RequestId;
use app\exceptions\NotFoundException;
use Ramsey\Uuid\Uuid;

class ARRequestRepository extends BaseRequestRepository
{
    public function get(RequestId $id)
    {
        if (!$request = Request::findOne($id->getValue())) {
            throw new NotFoundException('Request not found.');
        }
        return $request;
    }

    public function all(array $conditions = [])
    {
        return Request::findAll($conditions);
    }

    public function add(Request $request)
    {
        if (!$request->insert()) {
            throw new \RuntimeException('Adding error.');
        }
    }

    public function save(Request $request)
    {
        if (!$request->update()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Request $request)
    {
        if (!$request->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    public function nextId()
    {
        return Uuid::uuid4()->toString();
    }
}