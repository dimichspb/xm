<?php
namespace app\repositories;

use app\entities\request\Request;
use app\entities\request\RequestId;

interface RequestRepositoryInterface
{
    public function get(RequestId $id);
    public function all(array $conditions = []);
    public function add(Request $request);
    public function save(Request $request);
    public function remove(Request $request);
    public function nextId();

}