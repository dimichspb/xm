<?php
$db = require __DIR__ . '/db.php';
// test database! Important not to run tests on production or development databases
$db['dsn'] = 'mysql:host=localhost;dbname=xm_test';
$db['username'] = 'xm_test';
$db['password'] = 'xm_test';

return $db;
