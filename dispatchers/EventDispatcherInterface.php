<?php
namespace app\dispatchers;

interface EventDispatcherInterface
{
    public function dispatch(array $events);
}