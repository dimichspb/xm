<?php

namespace app\bootstrap;

use app\dispatchers\EventDispatcherInterface;
use app\dispatchers\DummyEventDispatcher;
use app\factories\QuoteFactory;
use app\factories\QuoteFactoryInterface;
use app\factories\SymbolFactory;
use app\factories\SymbolFactoryInterface;
use app\repositories\APIQuoteRepository;
use app\repositories\ARRequestRepository;
use app\repositories\ARSymbolRepository;
use app\repositories\QuoteRepositoryInterface;
use app\repositories\RequestRepositoryInterface;
use app\factories\RequestFactory;
use app\factories\RequestFactoryInterface;
use app\repositories\SymbolRepositoryInterface;
use app\services\QuoteService;
use app\services\QuoteServiceInterface;
use app\services\SymbolProviderService;
use app\services\RequestService;
use app\services\RequestServiceInterface;
use app\services\SymbolProviderServiceInterface;
use app\services\SymbolService;
use app\services\SymbolServiceInterface;
use ProxyManager\Factory\LazyLoadingValueHolderFactory;
use yii\base\BootstrapInterface;

class WebContainerBootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $container = \Yii::$container;

        $container->setSingleton(EventDispatcherInterface::class, DummyEventDispatcher::class);

        $container->setSingleton(RequestRepositoryInterface::class, ARRequestRepository::class);

        $container->setSingleton(RequestServiceInterface::class, RequestService::class);

        $container->setSingleton(RequestFactoryInterface::class, RequestFactory::class);

        $container->setSingleton(SymbolRepositoryInterface::class, ARSymbolRepository::class);

        $container->setSingleton(SymbolServiceInterface::class, SymbolService::class);

        $container->setSingleton(SymbolFactoryInterface::class, SymbolFactory::class);

        $container->setSingleton(QuoteRepositoryInterface::class, APIQuoteRepository::class, [
            'https://finance.google.com/finance/historical?output=csv',
            'get',
            'spbsrv-proxy2.t-systems.ru:3128'
        ]);

        $container->setSingleton(QuoteServiceInterface::class, QuoteService::class);

        $container->setSingleton(QuoteFactoryInterface::class, QuoteFactory::class);
    }
}