<?php

namespace tests\models;

use app\forms\LoginForm;
use app\forms\RequestCreateForm;
use app\tests\_fixtures\SymbolFixture;
use Codeception\Specify;

class RequestFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    public $tester;

    private $model;

    public function _before()
    {
        $this->tester->haveFixtures([
            'symbols' => SymbolFixture::className(),
        ]);
    }

    public function testSuccess()
    {
        $this->model = new RequestCreateForm();
        $this->model->symbol_id = '1';
        $this->model->start_date = '2017-01-01';
        $this->model->end_date = '2017-01-02';
        $this->model->email = 'test@example.com';

        expect_that($this->model->validate());
    }
    public function testNoSymbolId()
    {
        $this->model = new RequestCreateForm();
        $this->model->start_date = '2017-01-01';
        $this->model->end_date = '2017-01-02';
        $this->model->email = 'test@example.com';

        expect_not($this->model->validate());
    }
}
