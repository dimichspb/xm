<?php
namespace tests\unit\repositories;
use app\repositories\ARRequestRepository;
use app\tests\_fixtures\RequestFixture;
use app\tests\_fixtures\SymbolFixture;


class ARRequestRepositoryTest extends BaseRequestRepositoryTest
{
    /**
     * @var \UnitTester
     */
    public $tester;
    public function _before()
    {
        $this->tester->haveFixtures([
            'requests' => RequestFixture::className(),
        ]);

        $this->repository = new ARRequestRepository();
    }
}