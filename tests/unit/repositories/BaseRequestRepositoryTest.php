<?php

namespace tests\unit\repositories;

use app\entities\request\Request;
use app\entities\request\RequestId;
use app\entities\symbol\SymbolId;
use app\entities\request\StartDate;
use app\entities\request\EndDate;
use app\entities\Email;
use app\repositories\ARRequestRepository;
use app\repositories\BaseRequestRepository;
use app\exceptions\NotFoundException;
use tests\unit\entities\Request\RequestBuilder;
use Codeception\Test\Unit;

abstract class BaseRequestRepositoryTest extends Unit
{
    /**
     * @var BaseRequestRepository
     */
    protected $repository;

    public function testGet()
    {
        $request = new Request(
            new RequestId('1'),
            new SymbolId('1'),
            new StartDate('2017-01-01 00:00:00'),
            new EndDate('2017-01-02 00:00:00'),
            new Email('test@example.com')
        );

        $this->repository->add($request);

        /** @var Request $found */
        $found = $this->repository->get($request->getId());

        $this->assertNotNull($found);
        $this->assertEquals($request->getId(), $found->getId());
    }

    public function testGetNotFound()
    {
        $this->expectException(NotFoundException::class);

        $this->repository->get(new RequestId(25));
    }

    public function testAdd()
    {
        $request = new Request(
            new RequestId('1'),
            new SymbolId('1'),
            new StartDate('2017-01-01 00:00:00'),
            new EndDate('2017-01-02 00:00:00'),
            new Email('test@example.com')
        );

        $this->repository->add($request);

        /** @var Request $found */
        $found = $this->repository->get($request->getId());

        $this->assertEquals($request->getId(), $found->getId());
        $this->assertEquals($request->getSymbolId(), $found->getSymbolId());
        $this->assertEquals($request->getStartDate(), $found->getStartDate());
        $this->assertEquals($request->getEndDate(), $found->getEndDate());
        $this->assertEquals($request->getEmail(), $found->getEmail());
    }

    public function testSave()
    {
        $request = new Request(
            new RequestId('1'),
            new SymbolId('1'),
            new StartDate('2017-01-01 00:00:00'),
            new EndDate('2017-01-02 00:00:00'),
            new Email('test@example.com')
        );

        $this->repository->add($request);

        /** @var Request $edit */
        $edit = $this->repository->get($request->getId());

        $edit->setEmail(new Email('test2@example.com'));

        $this->repository->save($edit);

        /** @var Request $found */
        $found = $this->repository->get($edit->getId());

        $this->assertEquals($request->getId(), $found->getId());
        $this->assertEquals($request->getSymbolId(), $found->getSymbolId());
        $this->assertEquals($request->getStartDate(), $found->getStartDate());
        $this->assertEquals($request->getEndDate(), $found->getEndDate());
        $this->assertNotEquals($request->getEmail(), $found->getEmail());
    }

    public function testRemove()
    {
        $request = new Request(
            new RequestId('1'),
            new SymbolId('1'),
            new StartDate('2017-01-01 00:00:00'),
            new EndDate('2017-01-02 00:00:00'),
            new Email('test@example.com')
        );

        $this->repository->add($request);

        $this->repository->remove($request);

        $this->expectException(NotFoundException::class);

        $this->repository->get(new RequestId(25));
    }

}