<?php
namespace tests\unit\entities\request;

use app\entities\Email;
use app\entities\request\EndDate;
use app\entities\request\Request;
use app\entities\request\RequestId;
use app\entities\request\StartDate;
use app\entities\symbol\SymbolId;
use Codeception\Test\Unit;

class RequestCreateTest extends Unit
{
    public function testSuccess()
    {
        $request = new Request(
            $id = new RequestId('1'),
            $symbolId = new SymbolId('1'),
            $startDate = new StartDate('2017-01-01'),
            $endDate = new EndDate('2017-01-02'),
            $email = new Email('test@example.com'));

        $this->assertEquals($id, $request->getId());
        $this->assertEquals($symbolId, $request->getSymbolId());
        $this->assertEquals($startDate, $request->getStartDate());
        $this->assertEquals($endDate, $request->getEndDate());
        $this->assertEquals($email, $request->getEmail());
    }
}