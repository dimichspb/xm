<?php
namespace tests\unit\entities\request;

use app\entities\Email;
use app\entities\request\EndDate;
use app\entities\request\Request;
use app\entities\request\RequestId;
use app\entities\request\StartDate;
use app\entities\symbol\SymbolId;

class RequestBuilder
{
    private $id = 1;
    private $symbol_id;
    private $start_date;
    private $end_date;
    private $email;

    public static function instance()
    {
        return new self();
    }
    public function withId($id)
    {
        $this->id = $id;
        return $this;
    }
    public function withSymbolId($id)
    {
        $this->symbol_id = $id;
        return $this;
    }
    public function withStartDate($startDate)
    {
        $this->start_date = $startDate;
        return $this;
    }
    public function withEndDate($endDate)
    {
        $this->end_date = $endDate;
        return $this;
    }
    public function withEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    public function build()
    {
        $request = new Request(
            new RequestId($this->id),
            new SymbolId($this->symbol_id),
            new StartDate($this->start_date),
            new EndDate($this->end_date),
            new Email($this->email)
        );
        return $request;
    }
}