<?php
namespace tests\unit\entities\request;

use app\entities\request\RequestId;
use Assert\InvalidArgumentException;
use Codeception\Test\Unit;

class RequestIdCreateTest extends Unit
{
    public function testSuccess()
    {
        $requestId = new RequestId('1');

        $this->assertEquals('1', $requestId->getValue());
    }

    public function testEmpty()
    {
        $this->expectException(InvalidArgumentException::class);

        new RequestId();
    }
}