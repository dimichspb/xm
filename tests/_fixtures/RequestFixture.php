<?php
namespace app\tests\_fixtures;

use yii\test\ActiveFixture;

class RequestFixture extends ActiveFixture
{
    public $modelClass = 'app\entities\request\Request';
    public $dataFile = '@tests/_fixtures/data/request.php';
}