<?php
namespace app\tests\_fixtures;

use yii\test\ActiveFixture;

class SymbolFixture extends ActiveFixture
{
    public $modelClass = 'app\entities\symbol\Symbol';
    public $dataFile = '@tests/_fixtures/data/symbol.php';
}