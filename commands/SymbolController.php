<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\entities\symbol\Symbol;
use app\forms\SymbolCreateForm;
use app\services\SymbolProviderServiceInterface;
use app\services\SymbolService;
use app\services\SymbolServiceInterface;
use yii\base\Module;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SymbolController extends Controller
{
    private $service;
    private $provider;

    public function __construct($id, Module $module, SymbolServiceInterface $service, SymbolProviderServiceInterface $provider, array $config = [])
    {
        $this->service = $service;
        $this->provider = $provider;

        parent::__construct($id, $module, $config);
    }

    /**
     * This command updates Symbols list.
     */
    public function actionUpdate()
    {
        $this->stdout('Starting update symbols from Provider...' . PHP_EOL);

        $added = 0;
        $data = $this->provider->findAll();

        if (!count($data)) {
            $this->stderr('No data was provided by Provider');
            return;
        }
        $this->stdout('Got total ' . count($data) . ' records from Provider' . PHP_EOL);

        foreach ($data as $item) {
            $form = new SymbolCreateForm();
            $form->code = $item->getCode()->getValue();
            $form->name = $item->getName()->getValue();

            if ($this->service->create($form)) {
                $added++;
                $this->stdout('Symbol ' . $form->code . ' has been created' . PHP_EOL);
            }
        }
        $this->stdout('Totally ' . $added . ' symbols have been created');
    }
}
