<?php
namespace app\forms;

use app\entities\quote\Quote;
use yii\base\Model;

class QuoteCreateForm extends Model
{
    public $symbol_id;
    public $date;
    public $open;
    public $high;
    public $low;
    public $close;
    public $volume;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['symbol_id', 'date', 'open', 'high', 'low', 'close', 'value'], 'required'],
            [['date'], 'date', 'format' => 'dd.MM.yyyy'],
            [['open', 'high', 'low', 'close', 'volume'], 'number',],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'symbol_id' => 'Symbol',
            'date' => 'Date',
            'open' => 'Open',
            'high' => 'High',
            'low'  => 'Low',
            'close' => 'Close',
            'volume' => 'Volume',
        ];
    }
}