<?php
namespace app\forms;

use app\entities\symbol\Symbol;
use nepstor\validators\DateTimeCompareValidator;
use yii\base\Model;

class RequestCreateForm extends Model
{
    public $symbol_id;
    public $start_date;
    public $end_date;
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['symbol_id', 'start_date', 'end_date', 'email'], 'required'],
            [['symbol_id'], 'string'],
            [['start_date', 'end_date'], 'date', 'format' => 'yyyy-MM-dd'],
            ['end_date', DateTimeCompareValidator::class, 'compareAttribute' => 'start_date', 'format' => 'Y-m-d', 'operator' => '>'],
            [['start_date', 'end_date'], 'default', 'value' => null],
            [['email'], 'email'],
            [['symbol_id'], 'exist', 'skipOnError' => true, 'targetClass' => Symbol::className(), 'targetAttribute' => ['symbol_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'symbol_id' => 'Symbol',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'email' => 'Email',
        ];
    }
}