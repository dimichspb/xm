<?php

namespace app\services;

use app\dto\QuoteCreateDto;
use app\entities\quote\QuoteId;
use app\entities\request\Request;
use app\factories\QuoteFactory;
use app\factories\QuoteFactoryInterface;
use app\forms\QuoteCreateForm;
use app\repositories\BaseQuoteRepository;
use app\repositories\QuoteRepositoryInterface;
use app\dispatchers\EventDispatcherInterface;

class QuoteService implements QuoteServiceInterface
{
    /**
     * @var QuoteRepositoryInterface
     */
    private $repository;

    /**
     * @var QuoteFactoryInterface
     */
    private $factory;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(QuoteRepositoryInterface $repository, QuoteFactoryInterface $factory, EventDispatcherInterface $dispatcher)
    {
        $this->repository = $repository;
        $this->factory = $factory;
        $this->dispatcher = $dispatcher;
    }

    public function create(QuoteCreateForm $form)
    {
        $dto = new QuoteCreateDto();
        $dto->id = $this->repository->nextId();
        $dto->symbol_id = $form->symbol_id;
        $dto->date = $form->date;
        $dto->open = $form->open;
        $dto->high = $form->high;
        $dto->low = $form->low;
        $dto->close = $form->close;
        $dto->volume = $form->volume;

        $quote = $this->factory->create($dto);

        $this->repository->add($quote);
        $this->dispatcher->dispatch($quote->releaseEvents());

        return $quote;
    }

    public function findOne(QuoteId $id)
    {
        return $this->repository->get($id);
    }

    public function findAll(Request $request)
    {
        return $this->repository->all($request);
    }
}