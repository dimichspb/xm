<?php
namespace app\services;

use app\dispatchers\EventDispatcherInterface;
use app\entities\symbol\SymbolId;
use app\exceptions\NotFoundException;
use app\exceptions\ProviderNotAvailableException;
use app\factories\SymbolFactoryInterface;
use app\repositories\SymbolProviderRepositoryInterface;
use app\repositories\SymbolRepositoryInterface;
use yii\base\Component;
use yii\httpclient\Client;

class SymbolProviderService implements SymbolProviderServiceInterface
{
    private $repository;
    private $factory;
    private $dispatcher;

    public function __construct(SymbolProviderRepositoryInterface $repository, SymbolFactoryInterface $factory, EventDispatcherInterface $dispatcher)
    {
        $this->repository = $repository;
        $this->factory = $factory;
        $this->dispatcher = $dispatcher;
    }

    public function findOne(SymbolId $id)
    {
        return $this->repository->get($id);
    }

    public function findAll(array $conditions = [])
    {
        return $this->repository->all($conditions);
    }
}