<?php
namespace app\services;

use app\entities\request\Request;
use app\entities\request\RequestId;
use app\forms\RequestCreateForm;

interface RequestServiceInterface
{
    /**
     * @param RequestCreateForm $form
     * @return Request|null
     */
    public function create(RequestCreateForm $form);
    public function findOne(RequestId $id);
    public function findAll(array $conditions = []);
}