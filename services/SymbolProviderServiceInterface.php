<?php
namespace app\services;

use app\entities\symbol\Symbol;
use app\entities\symbol\SymbolId;
use app\forms\SymbolCreateForm;

interface SymbolProviderServiceInterface
{
    public function findOne(SymbolId $id);

    /**
     * @param array $conditions
     * @return Symbol[]
     */
    public function findAll(array $conditions = []);
}