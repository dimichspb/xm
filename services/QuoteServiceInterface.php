<?php
namespace app\services;

use app\entities\quote\QuoteId;
use app\entities\request\Request;
use app\forms\QuoteCreateForm;

interface QuoteServiceInterface
{
    public function create(QuoteCreateForm $request);
    public function findOne(QuoteId $id);
    public function findAll(Request $request);
}