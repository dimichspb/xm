<?php

namespace app\services;

use app\dto\SymbolCreateDto;
use app\entities\symbol\SymbolId;
use app\factories\SymbolFactory;
use app\factories\SymbolFactoryInterface;
use app\forms\SymbolCreateForm;
use app\repositories\BaseSymbolRepository;
use app\repositories\SymbolRepositoryInterface;
use app\dispatchers\EventDispatcherInterface;

class SymbolService implements SymbolServiceInterface
{
    /**
     * @var SymbolRepositoryInterface
     */
    private $repository;

    /**
     * @var SymbolFactoryInterface
     */
    private $factory;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(SymbolRepositoryInterface $repository, SymbolFactoryInterface $factory, EventDispatcherInterface $dispatcher)
    {
        $this->repository = $repository;
        $this->factory = $factory;
        $this->dispatcher = $dispatcher;
    }

    public function create(SymbolCreateForm $form)
    {
        if ($this->repository->all(['code' => $form->code])) {
            return null;
        }

        $dto = new SymbolCreateDto();
        $dto->id = $this->repository->nextId();
        $dto->code = $form->code;
        $dto->name = $form->name;

        $symbol = $this->factory->create($dto);

        $this->repository->add($symbol);
        $this->dispatcher->dispatch($symbol->releaseEvents());

        return $symbol;
    }

    public function findOne(SymbolId $id)
    {
        return $this->repository->get($id);
    }

    public function findAll(array $conditions = [])
    {
        return $this->repository->all($conditions);
    }
}