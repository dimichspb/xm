<?php

namespace app\services;

use app\dto\RequestCreateDto;
use app\entities\request\RequestId;
use app\factories\RequestFactory;
use app\factories\RequestFactoryInterface;
use app\forms\RequestCreateForm;
use app\repositories\BaseRequestRepository;
use app\repositories\RequestRepositoryInterface;
use app\dispatchers\EventDispatcherInterface;

class RequestService implements RequestServiceInterface
{
    /**
     * @var RequestRepositoryInterface
     */
    private $repository;

    /**
     * @var RequestFactoryInterface
     */
    private $factory;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(RequestRepositoryInterface $repository, RequestFactoryInterface $factory, EventDispatcherInterface $dispatcher)
    {
        $this->repository = $repository;
        $this->factory = $factory;
        $this->dispatcher = $dispatcher;
    }

    public function create(RequestCreateForm $form)
    {
        $dto = new RequestCreateDto();

        $dto->id = $this->repository->nextId();
        $dto->symbol_id = $form->symbol_id;
        $dto->start_date = $form->start_date;
        $dto->end_date = $form->end_date;
        $dto->email = $form->email;

        $request = $this->factory->create($dto);

        $this->repository->add($request);
        $this->dispatcher->dispatch($request->releaseEvents());

        return $request;
    }

    public function findOne(RequestId $id)
    {
        return $this->repository->get($id);
    }

    public function findAll(array $conditions = [])
    {
        return $this->repository->all($conditions);
    }
}