<?php
namespace app\services;

use app\entities\symbol\SymbolId;
use app\forms\SymbolCreateForm;

interface SymbolServiceInterface
{
    public function create(SymbolCreateForm $request);
    public function findOne(SymbolId $id);
    public function findAll(array $conditions = []);
}