<?php
namespace app\dto;

class RequestCreateDto
{
    public $id;
    public $symbol_id;
    public $start_date;
    public $end_date;
    public $email;
}