<?php
namespace app\dto;

class SymbolCreateDto
{
    public $id;
    public $code;
    public $name;
}