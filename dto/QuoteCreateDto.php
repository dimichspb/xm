<?php
namespace app\dto;

class QuoteCreateDto
{
    public $id;
    public $symbol_id;
    public $date;
    public $open;
    public $high;
    public $low;
    public $close;
    public $volume;
}