<?php
namespace app\controllers;

use app\entities\symbol\Symbol;
use app\services\SymbolServiceInterface;
use yii\base\Module;
use yii\data\ArrayDataProvider;
use yii\rest\ActiveController;
use yii\web\Request;

class SymbolController extends ActiveController
{
    public $modelClass = Symbol::class;

    private $service;
    private $request;

    public function __construct($id, Module $module, SymbolServiceInterface $service, Request $request, array $config = [])
    {
        $this->service = $service;
        $this->request = $request;

        parent::__construct($id, $module, $config);
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['create'], $actions['update'], $actions['view']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $search = $this->request->get('q');

        $dataProvider = new ArrayDataProvider();
        $conditions = $search? ['like', 'code', $search]: [];
        $dataProvider->allModels = $this->service->findAll($conditions);

        return $dataProvider;
    }
}