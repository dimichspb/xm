<?php

namespace app\controllers;

use app\entities\quote\Quote;
use app\entities\request\RequestId;
use app\entities\symbol\SymbolId;
use app\forms\RequestCreateForm;
use app\services\QuoteServiceInterface;
use app\services\RequestServiceInterface;
use app\services\SymbolServiceInterface;
use Yii;
use yii\base\Module;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\mail\MailerInterface;
use yii\web\NotFoundHttpException;
use yii\web\Request;

class ExerciseController extends \yii\web\Controller
{
    private $requestService;
    private $quoteService;
    private $symbolService;
    private $request;
    private $mailer;

    public function __construct($id, Module $module, RequestServiceInterface $requestService,
                                SymbolServiceInterface $symbolService, QuoteServiceInterface $quoteService,
                                Request $request, array $config = [])
    {
        $this->requestService = $requestService;
        $this->quoteService = $quoteService;
        $this->symbolService = $symbolService;
        $this->request = $request;
        $this->mailer = Yii::$app->mailer;

        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'email' => ['post'],
                ],
            ],
        ];
    }

    public function actionStart()
    {
        $model = new RequestCreateForm();
        $symbolDataProvider = new ArrayDataProvider();
        $symbolDataProvider->allModels = $this->symbolService->findAll();

        if ($this->request->isPost && $model->load($this->request->post()) && $model->validate()) {
            $request = $this->requestService->create($model);
            $this->redirect(['result', 'id' => $request->getId()->getValue()]);
        }

        $selectedSymbol = $model->symbol_id? $this->symbolService->findOne(new SymbolId($model->symbol_id)): null;

        return $this->render('start', [
            'model' => $model,
            'symbolDataProvider' => $symbolDataProvider,
            'selectedSymbol' => $selectedSymbol,
        ]);
    }

    public function actionResult($id)
    {
        $model = $this->findModel($id);

        $dataProvider = new ArrayDataProvider();

        $dataProvider->allModels = $this->quoteService->findAll($model);

        return $this->render('result', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionChart($id)
    {
        $model = $this->findModel($id);

        /** @var Quote[] $quotes */
        $quotes = $this->quoteService->findAll($model);

        $data = [];

        $data[] = [
            'Date',
            'Low',
            'Open',
            'Close',
            'High',
        ];

        foreach ($quotes as $quote) {
            $data[] = [
                $quote->getDate()->getFormattedValue(),
                (float)$quote->getLow()->getValue(),
                (float)$quote->getOpen()->getValue(),
                (float)$quote->getClose()->getValue(),
                (float)$quote->getHigh()->getValue(),
            ];
        }

        return $this->render('chart', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionEmail($id)
    {
        $model = $this->findModel($id);

        $body = '"From '. $model->getStartDate()->getShortDate() . ' to ' . $model->getEndDate()->getShortDate() . '"';

        $result = $this->mailer->compose()
            ->setTo($model->getEmail()->getValue())
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject($model->symbol->getName()->getValue())
            ->setTextBody($body)
            ->send();

        if (!$result) {
            return $this->render('email-error', [
                'model' => $model,
            ]);
        }

        return $this->render('email-success', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \app\entities\request\Request
     * @throws NotFoundHttpException
     */
    public function findModel($id)
    {
        $model = $this->requestService->findOne(new RequestId($id));

        if (!$model) {
            throw new NotFoundHttpException('Request not found');
        }

        return $model;
    }
}
