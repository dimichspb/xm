<?php
namespace app\factories;

use app\dto\RequestCreateDto;
use app\entities\Email;
use app\entities\request\EndDate;
use app\entities\request\Request;
use app\entities\request\RequestId;
use app\entities\request\StartDate;
use app\entities\symbol\SymbolId;

class RequestFactory implements RequestFactoryInterface
{
    public function create(RequestCreateDto $dto)
    {
        $request = new Request(
            new RequestId($dto->id),
            new SymbolId($dto->symbol_id),
            new StartDate($dto->start_date),
            new EndDate($dto->end_date),
            new Email($dto->email)
        );

        return $request;
    }
}