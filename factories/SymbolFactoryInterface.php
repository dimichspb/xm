<?php
namespace app\factories;

use app\dto\SymbolCreateDto;
use app\entities\symbol\Symbol;

interface SymbolFactoryInterface
{
    /**
     * @param SymbolCreateDto $dto
     * @return Symbol
     */
    public function create(SymbolCreateDto $dto);
}