<?php
namespace app\factories;

use app\dto\RequestCreateDto;
use app\entities\request\Request;

interface RequestFactoryInterface
{
    /**
     * @param RequestCreateDto $dto
     * @return Request
     */
    public function create(RequestCreateDto $dto);
}