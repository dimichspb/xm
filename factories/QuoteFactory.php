<?php
namespace app\factories;

use app\dto\QuoteCreateDto;
use app\entities\quote\Close;
use app\entities\quote\Date;
use app\entities\quote\High;
use app\entities\quote\Low;
use app\entities\quote\Open;
use app\entities\quote\Quote;
use app\entities\quote\QuoteId;
use app\entities\quote\Volume;
use app\entities\symbol\SymbolId;

class QuoteFactory implements QuoteFactoryInterface
{
    public function create(QuoteCreateDto $dto)
    {
        $quote = new Quote(
            new QuoteId($dto->id),
            new SymbolId($dto->symbol_id),
            new Date($dto->date),
            new Open($dto->open),
            new High($dto->high),
            new Low($dto->low),
            new Close($dto->close),
            new Volume($dto->volume)
        );

        return $quote;
    }
}