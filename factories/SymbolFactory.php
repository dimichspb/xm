<?php
namespace app\factories;

use app\dto\SymbolCreateDto;
use app\entities\symbol\Code;
use app\entities\symbol\Name;
use app\entities\symbol\Symbol;
use app\entities\symbol\SymbolId;

class SymbolFactory implements SymbolFactoryInterface
{
    public function create(SymbolCreateDto $dto)
    {
        $symbol = new Symbol(
            new SymbolId($dto->id),
            new Code($dto->code),
            new Name($dto->name)
        );

        return $symbol;
    }
}