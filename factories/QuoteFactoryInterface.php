<?php
namespace app\factories;

use app\dto\QuoteCreateDto;
use app\entities\quote\Quote;

interface QuoteFactoryInterface
{
    /**
     * @param QuoteCreateDto $dto
     * @return Quote
     */
    public function create(QuoteCreateDto $dto);
}