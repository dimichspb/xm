XM.com

PHP Exercise v17.0.2


Installation:

1. git clone https://dimichspb@bitbucket.org/dimichspb/xm.git
2. php composer.phar install
3. set db details in config/db.php
4. php yii migrate
5. php yii symbol/update
6. php yii serve
7. go to http://localhost:8080/
8. have fun

Tests:

1. make all installation steps
2. set test-db details in config/test_db.php
3. tests/bin/yii migrate
4. vendor/bin/codecept run


NB: Emails are stored in runtime/mail